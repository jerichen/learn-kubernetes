## 進容器查看 secret
````
env | grep DB_USERNAME
````
````
env | grep DB_PASSWORD
````

## 印出 secret yaml
````
$ kubectl get secret mysecret --output=yaml
````

## docker private image
````
kubectl create secret docker-registry regsecret --docker-server=https://index.docker.io/v1/ --docker-username={docker-name} --docker-password={docker-password} --docker-email={docker-email}
````
### output secret
$ kubectl get secret regsecret -o yaml

### secret from file
````
$ kubectl create secret generic my-secret --from-file=./username.txt --from-file=./password.txt
````
````
$ kubectl create secret generic demo-secret-from-literal \
> --from-literal=username=admin \
> --from-literal=password=password
````