$ for i in 0 1; do kubectl exec web-$i -- sh -c 'echo hello $(hostname) > /usr/share/nginx/html/index.html'; done
$ for i in 0 1; do kubectl exec -it web-$i -- curl localhost; done

$ kubectl get pods -w -l app=nginx
$ kubectl scale sts web --replicas=3

$ kubectl get pods -w
