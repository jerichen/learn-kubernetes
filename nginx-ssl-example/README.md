## 套用 SSL

##### 透過指令建立金鑰與憑證
````
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx.key -out nginx.crt
````
##### secret from file
````
$ kubectl create secret generic ssl-cert --from-file=./nginx.key --from-file=./nginx.crt
````

##### config from configmap
````
$ kubectl create configmap default-config --from-file=default.conf
````


